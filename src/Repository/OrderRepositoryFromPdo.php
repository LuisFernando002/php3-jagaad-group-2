<?php

namespace Project\Repository;

use Project\Entity\Order;
use PDO;

class OrderRepositoryFromPdo implements OrderRepository
{
    public function __construct(private PDO $pdo)
    {
    }
    public function createOrder(Order $order): void
    {
        $stmt = $this->pdo->prepare(<<<SQL
        INSERT INTO orders
        (id, order_total, order_checkout_date)
        VALUES
        (:id, :order_total, :order_checkout_date)
        SQL);

        $orderId = $order->id();

        $params = [
            ':id' => $orderId,
            ':order_total' => $order->order_total(),
            ':order_checkout_date' => $order->order_checkout_date(),
        ];

        $stmt->execute($params);
    }
    public function getAllOrders(): array
    {
        $stmt = $this->pdo->prepare(<<<SQL
        SELECT * FROM orders
        SQL);

        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Order::class);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
