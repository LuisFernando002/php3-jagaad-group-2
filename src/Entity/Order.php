<?php

namespace Project\Entity;

class Order
{
    private ?string $id;

    public function __construct(
        ?string $id = '',
        private float $order_total = 0,
        private string $order_checkout_date = '',
    ) {
        $this->id = $id;
        $this->order_total = $order_total;
        $this->order_checkout_date = $order_checkout_date;
    }
    public function id(): string
    {
        return $this->id;
    }
    public function order_total(): float
    {
        return $this->order_total;
    }
    public function order_checkout_date(): string
    {
        return $this->order_checkout_date;
    }
}
