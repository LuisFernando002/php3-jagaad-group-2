<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/modern-normalize">

    <title>Beverage Shop</title>
</head>

<body>

    <h1>Best Team Beverage Shop</h1>

    <div>
        <a href="index.php">Go to Create and List Beverages</a> |
        <a href="index.php?action=display_catalogue">Go to Beverage Catalogue</a> |
        <a href="index.php?action=DisplayCart">Go to Cart</a> |
        <a href="index.php?action=displayOrder">Go to Display Orders</a>  
    </div>
    <hr />
    <br>