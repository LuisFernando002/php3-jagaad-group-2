CREATE DATABASE IF NOT EXISTS beverage_shop; 

USE beverage_shop;

CREATE TABLE IF NOT EXISTS products
(
    id varchar(255) NOT NULL,
    product_name varchar (80) NOT NULL,
    product_description text,
    product_price float,
    product_quantity int,
    PRIMARY KEY(id)
);
CREATE TABLE IF NOT EXISTS orders
(
    id varchar(255) not null, 
    order_total float,
    order_checkout_date date,
    PRIMARY KEY(id)
   
);
CREATE TABLE IF NOT EXISTS order_products
(
    order_id varchar(255),
    product_id varchar(255),
    PRIMARY KEY (order_id, product_id),
    FOREIGN KEY (order_id) REFERENCES orders(id),
    FOREIGN KEY (product_id) REFERENCES products(id)
);

